CFLAGS = -std=gnu++0x -march=native -mtune=native -fexceptions
CFLAGS += -I$(ROOTSYS)/include/root -L$(ROOTSYS)/lib/root
CFLAGS += -lCore -lHist -lRIO -lTree -lGpad

ifdef RELEASE
	CFLAGS += -g3 -O2 -D_FORTIFY_SOURCE=1 -Werror=implicit-function-declaration
endif
ifndef RELEASE
	CFLAGS += -g3 -Og -D_FORTIFY_SOURCE=2
endif

wrapper.o: wrapper.cpp wrapper.hpp config.hpp
	$(CXX) -c $(CFLAGS) $< -o $@

clean:
	rm -rf *.o *.gch *.dSYM
